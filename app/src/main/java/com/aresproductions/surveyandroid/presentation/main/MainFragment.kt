package com.aresproductions.surveyandroid.presentation.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.aresproductions.surveyandroid.R
import com.aresproductions.surveyandroid.presentation.core.RequestState
import com.aresproductions.surveyandroid.presentation.core.observe
import com.aresproductions.surveyandroid.presentation.core.skipNull
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_main.*

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {
    private val viewModel: MainVM by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerButtons()
        viewModel.run {
            observe(requestState, ::handleRequestState.skipNull())
            syncQuestions()
        }
    }

    private fun registerButtons() {
        button_start_survey.setOnClickListener {
            findNavController().navigate(MainFragmentDirections.actionMainFragmentToQuestionFragment())
        }

        button_try_again.setOnClickListener {
            viewModel.syncQuestions()
        }
    }

    private fun handleRequestState(requestState: RequestState) {
        text_state.text = when (requestState) {
            RequestState.SUCCESS -> "Ready!"
            RequestState.FAIL -> "Failed, try again!"
            RequestState.IN_PROGRESS -> "In progress..."
            RequestState.IDLE -> ""
        }

        button_start_survey.isEnabled = when (requestState) {
            RequestState.SUCCESS -> true
            else -> false
        }

        button_try_again.visibility = when (requestState) {
            RequestState.FAIL -> View.VISIBLE
            else -> View.GONE
        }
    }
}