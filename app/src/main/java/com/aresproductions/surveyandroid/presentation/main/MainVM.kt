package com.aresproductions.surveyandroid.presentation.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.usecase.ISyncQuestionsUC
import com.aresproductions.surveyandroid.presentation.core.RequestState
import com.aresproductions.surveyandroid.presentation.core.immutable
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainVM @Inject constructor(
        private val syncQuestionsUC: ISyncQuestionsUC,
) : ViewModel() {

    private val _requestState = MutableLiveData(RequestState.IDLE)
    val requestState = _requestState.immutable()

    fun syncQuestions() {
        viewModelScope.launch(Dispatchers.IO) {
            _requestState.postValue(RequestState.IN_PROGRESS)
            when (syncQuestionsUC.execute()) {
                is Result.Success -> {
                    _requestState.postValue(RequestState.SUCCESS)
                }
                is Result.Error -> {
                    _requestState.postValue(RequestState.FAIL)
                }
            }
        }
    }

}