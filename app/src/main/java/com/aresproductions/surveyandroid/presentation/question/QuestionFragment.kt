package com.aresproductions.surveyandroid.presentation.question

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.aresproductions.surveyandroid.R
import com.aresproductions.surveyandroid.domain.entities.QuestionWithAnswer
import com.aresproductions.surveyandroid.presentation.core.RequestState
import com.aresproductions.surveyandroid.presentation.core.observe
import com.aresproductions.surveyandroid.presentation.core.observeCombined
import com.aresproductions.surveyandroid.presentation.core.skipNull
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_question.*


@AndroidEntryPoint
class QuestionFragment : Fragment(R.layout.fragment_question) {
    private val viewModel: QuestionVM by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.run {
            observe(answersCount, ::handleAnswersCount.skipNull())
            observeCombined(currentIndex, questionsCount, ::handleAnswersQuestionsCount.skipNull())
            observe(currentIndex, ::handleCurrentIndex.skipNull())
            observe(currentQnA, ::handleCurrentQnA.skipNull())
            observe(postRequestState, ::handlePostAnswerState.skipNull())
            observe(nextButtonIsEnabled, ::handleNextButtonState.skipNull())
            observe(previousButtonIsEnabled, ::handlePreviousButtonState.skipNull())
            observe(questionsWithAnswers, {
                viewModel.updateQna(currentIndex.value ?: 0)
            })

        }

        enableDisableSubmitButtonBasedOnEditText()
        setupButtonListeners()

    }

    private fun handleNextButtonState(isEnabled: Boolean) {
        button_next.isEnabled = isEnabled
    }

    private fun handlePreviousButtonState(isEnabled: Boolean) {
        button_previous.isEnabled = isEnabled
    }

    private fun setupButtonListeners() {
        button_submit.setOnClickListener {
            viewModel.postAnswer(edit_text_answer.text.toString())
        }
        button_next.setOnClickListener {
            viewModel.nextClicked()
        }

        button_previous.setOnClickListener {
            viewModel.previousClicked()
        }
    }

    private fun handleCurrentIndex(currentIndex: Int) {
        viewModel.updateQna(currentIndex)
    }

    private fun enableDisableSubmitButtonBasedOnEditText() {
        edit_text_answer.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                button_submit.isEnabled = s.isNotEmpty()
            }
        })
    }

    private fun handleAnswersQuestionsCount(currentQuestion: Int, questionsCount: Int) {
        text_question_number.text = "Question ${currentQuestion + 1}/$questionsCount"
        viewModel.updatePreviousNextButtonStates(currentQuestion)
    }

    private fun handleAnswersCount(count: Int) {
        text_answers_submitted.text = "Answers submitted: $count"
    }

    private fun handleCurrentQnA(qna: QuestionWithAnswer) {
        text_question.text = qna.question
        edit_text_answer.setText("")
        viewModel.resetPostState()

        if (qna.answer != null) {
            text_answer.text = qna.answer
            edit_text_answer.visibility = View.GONE
            text_answer.visibility = View.VISIBLE
            button_submit.visibility = View.GONE
        } else {
            text_answer.visibility = View.GONE
            button_submit.visibility = View.VISIBLE
            edit_text_answer.isEnabled = true
            button_submit.isEnabled = true
            edit_text_answer.visibility = View.VISIBLE
        }
    }

    private fun handlePostAnswerState(postState: RequestState) {
        when (postState) {
            RequestState.SUCCESS -> {
                button_submit.visibility = View.GONE
                edit_text_answer.visibility = View.GONE
                text_answer.visibility = View.VISIBLE
                text_post_state.text = "Success!!!"
                text_post_state.visibility = View.VISIBLE
            }
            RequestState.IN_PROGRESS -> {
                button_submit.visibility = View.VISIBLE
                button_submit.isEnabled = false
                edit_text_answer.isEnabled = false
                edit_text_answer.visibility = View.VISIBLE
                text_answer.visibility = View.GONE
                text_post_state.text = "Submitting..."
                text_post_state.visibility = View.VISIBLE
            }
            RequestState.FAIL -> {
                button_submit.visibility = View.VISIBLE
                button_submit.isEnabled = true
                edit_text_answer.isEnabled = true
                edit_text_answer.visibility = View.VISIBLE
                text_answer.visibility = View.GONE
                text_post_state.text = "Failed, try again..."
                text_post_state.visibility = View.VISIBLE
            }
            RequestState.IDLE -> {
                text_post_state.visibility = View.GONE
            }
        }
    }
}