package com.aresproductions.surveyandroid.presentation.core

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * In the context of a [LifecycleOwner]
 * When [LiveData] observer hits, the given [body] will be executed with its value as param
 */
fun <T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
        liveData.observe(this, { body(it) })

/**
 * In the context of a [LifecycleOwner]
 * Given 2 live data sources, when any of them hits,
 * param [body] will be executed with the given values of the sources
 */
fun <T1 : Any?, T2 : Any> LifecycleOwner.observeCombined(source1: LiveData<T1>, source2: LiveData<T2>, body: (T1?, T2?) -> Unit) {
    val combiner = MediatorLiveData<Pair<T1?, T2?>>()

    combiner.addSource(source1) {
        synchronized(combiner) {
            combiner.value = Pair(it, combiner.value?.second)
        }
    }

    combiner.addSource(source2) {
        synchronized(combiner) {
            combiner.value = Pair(combiner.value?.first, it)
        }
    }

    combiner.observe(this, { (v1, v2) -> body(v1, v2) })
}