package com.aresproductions.surveyandroid.presentation.core

enum class RequestState {
    IN_PROGRESS,
    SUCCESS,
    FAIL,
    IDLE
}