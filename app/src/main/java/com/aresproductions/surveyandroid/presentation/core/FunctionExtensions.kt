package com.aresproductions.surveyandroid.presentation.core

/**
 * Converts a function with a non-null parameter to a function with a nullable parameter.
 * In case of a given null parameter, the function will not be executed (skipped).
 */
fun <Param> ((Param) -> Unit).skipNull(): (Param?) -> Unit = { param ->
    if (param != null) {
        invoke(param)
    }
}

/**
 * Converts a function with non-null parameters to a function with a nullable parameters.
 * In case of a given null parameter, the function will not be executed (skipped).
 */
fun <Param1, Param2> ((Param1, Param2) -> Unit).skipNull(): (Param1?, Param2?) -> Unit = { param1, param2 ->
    if (param1 != null && param2 != null) {
        invoke(param1, param2)
    }
}