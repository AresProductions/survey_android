package com.aresproductions.surveyandroid.presentation.question

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.QuestionWithAnswer
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.usecase.IGetAllAnswersUC
import com.aresproductions.surveyandroid.domain.usecase.IGetQuestionsWithAnswersUC
import com.aresproductions.surveyandroid.domain.usecase.IPostAnswerUC
import com.aresproductions.surveyandroid.presentation.core.RequestState
import com.aresproductions.surveyandroid.presentation.core.immutable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class QuestionVM @ViewModelInject constructor(
        private val postAnswerUC: IPostAnswerUC,
        getAllAnswersUC: IGetAllAnswersUC,
        getQuestionsWithAnswersUC: IGetQuestionsWithAnswersUC
) : ViewModel() {

    private val _currentIndex = MutableLiveData(0)
    private val _currentQnA = MutableLiveData<QuestionWithAnswer>()
    private val _postRequestState = MutableLiveData(RequestState.IDLE)
    private val _nextButtonIsEnabled = MutableLiveData(false)
    private val _previousButtonIsEnabled = MutableLiveData(false)

    val questionsWithAnswers = getQuestionsWithAnswersUC.execute().asLiveData()
    val questionsCount = questionsWithAnswers.map { it.size }
    val answersCount = getAllAnswersUC.execute().asLiveData().map { it.size }
    val currentQnA = _currentQnA.immutable()
    val currentIndex = _currentIndex.immutable()
    val postRequestState = _postRequestState.immutable()
    val nextButtonIsEnabled = _nextButtonIsEnabled.immutable()
    val previousButtonIsEnabled = _previousButtonIsEnabled.immutable()

    fun nextClicked() {
        currentIndex.value?.let {
            _currentIndex.value = it + 1
        }
    }

    fun previousClicked() {
        currentIndex.value?.let {
            _currentIndex.value = it - 1
        }
    }

    fun postAnswer(answerDescription: String) {
        viewModelScope.launch(Dispatchers.IO) {
            (questionsWithAnswers.value?.getOrNull(currentIndex.value ?: -1))?.let {
                _postRequestState.postValue(RequestState.IN_PROGRESS)
                when (val result = postAnswerUC.execute(Answer(it.id, answerDescription))) {
                    is Result.Success -> {
                        _postRequestState.postValue(RequestState.SUCCESS)
                    }
                    is Result.Error -> {
                        Timber.e(result.throwable)
                        _postRequestState.postValue(RequestState.FAIL)
                    }
                }
            }
        }
    }

    fun updateQna(currentIndex: Int) {
        val newQnA = questionsWithAnswers.value?.getOrNull(currentIndex)
        _currentQnA.postValue(newQnA)
    }

    fun updatePreviousNextButtonStates(currentIndex: Int) {
        questionsCount.value?.let { questionsCount ->
            _previousButtonIsEnabled.postValue(currentIndex > 0)
            _nextButtonIsEnabled.postValue(currentIndex < questionsCount - 1)
        }
    }

    fun resetPostState() {
        _postRequestState.value = RequestState.IDLE
    }

}