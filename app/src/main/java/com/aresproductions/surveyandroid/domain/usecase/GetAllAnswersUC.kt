package com.aresproductions.surveyandroid.domain.usecase

import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface IGetAllAnswersUC {
    fun execute(): Flow<List<Answer>>
}

class GetAllAnswersUC @Inject constructor(
        private val answerRepo: IAnswerRepo
) : IGetAllAnswersUC {
    override fun execute(): Flow<List<Answer>> = answerRepo.getAll()
}
