package com.aresproductions.surveyandroid.domain.usecase

import com.aresproductions.surveyandroid.domain.entities.QuestionWithAnswer
import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import com.aresproductions.surveyandroid.domain.repository.IQuestionRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

interface IGetQuestionsWithAnswersUC {
    fun execute(): Flow<List<QuestionWithAnswer>>
}

class GetQuestionsWithAnswersUC @Inject constructor(
        private val answerRepo: IAnswerRepo,
        private val questionRepo: IQuestionRepo
) : IGetQuestionsWithAnswersUC {
    override fun execute(): Flow<List<QuestionWithAnswer>> {
        return questionRepo.getAll().combine(answerRepo.getAll()) { questions, answers ->
            questions.map { question ->
                val answer = answers.find { it.id == question.id }
                return@map QuestionWithAnswer(question.id, question.description, answer?.description)
            }
        }
    }
}