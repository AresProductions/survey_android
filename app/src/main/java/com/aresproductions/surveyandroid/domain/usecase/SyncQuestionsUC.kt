package com.aresproductions.surveyandroid.domain.usecase

import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.repository.IQuestionRepo
import javax.inject.Inject

interface ISyncQuestionsUC {
    suspend fun execute(): Result<Unit>
}

class SyncQuestionsUC @Inject constructor(
        private val questionRepo: IQuestionRepo
) : ISyncQuestionsUC {
    override suspend fun execute(): Result<Unit> = questionRepo.sync()
}