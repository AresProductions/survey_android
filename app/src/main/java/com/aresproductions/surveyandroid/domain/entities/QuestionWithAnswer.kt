package com.aresproductions.surveyandroid.domain.entities

data class QuestionWithAnswer(
        val id: Int,
        val question: String,
        val answer: String?
)