package com.aresproductions.surveyandroid.domain.entities

data class Question(
        val id: Int,
        val description: String
)