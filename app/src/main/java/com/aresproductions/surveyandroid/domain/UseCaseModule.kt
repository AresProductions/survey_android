package com.aresproductions.surveyandroid.domain

import com.aresproductions.surveyandroid.domain.usecase.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface UseCaseModule {
    @get:Binds
    val GetQuestionsWithAnswersUC.iGetQuestionsWithAnswersUC: IGetQuestionsWithAnswersUC

    @get:Binds
    val GetQuestionWithAnswerUC.iGetQuestionWithAnswerUC: IGetQuestionWithAnswerUC

    @get:Binds
    val GetAllAnswersUC.iGetAllAnswersUC: IGetAllAnswersUC

    @get:Binds
    val PostAnswerUC.iPostAnswerUC: IPostAnswerUC

    @get:Binds
    val SyncQuestionsUC.iSyncQuestionsUC: ISyncQuestionsUC
}