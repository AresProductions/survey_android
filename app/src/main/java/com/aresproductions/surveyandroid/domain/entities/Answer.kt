package com.aresproductions.surveyandroid.domain.entities

data class Answer(
        val id: Int,
        val description: String
)