package com.aresproductions.surveyandroid.domain.repository

import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import kotlinx.coroutines.flow.Flow

interface IAnswerRepo {
    suspend fun post(answer: Answer): Result<Unit>
    fun getAll(): Flow<List<Answer>>
    fun get(id: Int): Flow<Answer?>
}