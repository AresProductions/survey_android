package com.aresproductions.surveyandroid.domain.repository

import com.aresproductions.surveyandroid.domain.entities.Question
import com.aresproductions.surveyandroid.domain.entities.Result
import kotlinx.coroutines.flow.Flow

interface IQuestionRepo {
    suspend fun sync(): Result<Unit>
    fun getAll(): Flow<List<Question>>
    fun get(id: Int): Flow<Question?>
}