package com.aresproductions.surveyandroid.domain.usecase

import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import javax.inject.Inject

interface IPostAnswerUC {
    suspend fun execute(answer: Answer): Result<Unit>
}

class PostAnswerUC @Inject constructor(
        private val answerRepo: IAnswerRepo
) : IPostAnswerUC {
    override suspend fun execute(answer: Answer): Result<Unit> = answerRepo.post(answer)
}