package com.aresproductions.surveyandroid.domain.usecase

import com.aresproductions.surveyandroid.data.QuestionRepo
import com.aresproductions.surveyandroid.domain.entities.QuestionWithAnswer
import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

interface IGetQuestionWithAnswerUC {
    fun execute(id: Int): Flow<QuestionWithAnswer?>
}

class GetQuestionWithAnswerUC @Inject constructor(
        private val answerRepo: IAnswerRepo,
        private val questionRepo: QuestionRepo
) : IGetQuestionWithAnswerUC {
    override fun execute(id: Int): Flow<QuestionWithAnswer?> {
        return questionRepo.get(id).combine(answerRepo.get(id)) { question, answer ->
            if (question != null) {
                QuestionWithAnswer(id = id, question = question.description, answer = answer?.description)
            }
            null
        }
    }
}