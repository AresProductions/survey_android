package com.aresproductions.surveyandroid.data.remote

import com.aresproductions.surveyandroid.data.remote.model.RAnswer
import com.aresproductions.surveyandroid.data.remote.model.RQuestion
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface QuestionAnswerApiService {
    companion object {
        const val BASE_URL = "https://powerful-peak-54206.herokuapp.com/"
    }

    @GET("questions")
    suspend fun getQuestions(): List<RQuestion>

    @POST("question/submit")
    suspend fun postAnswer(@Body answer: RAnswer)

}