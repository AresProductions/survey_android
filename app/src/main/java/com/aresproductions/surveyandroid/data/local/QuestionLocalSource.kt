package com.aresproductions.surveyandroid.data.local

import com.aresproductions.surveyandroid.data.local.dao.DQuestionDao
import com.aresproductions.surveyandroid.data.local.model.toData
import com.aresproductions.surveyandroid.data.local.model.toDomain
import com.aresproductions.surveyandroid.domain.entities.Question
import com.aresproductions.surveyandroid.domain.entities.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import javax.inject.Inject

class QuestionLocalSource @Inject constructor(
        private val questionDao: DQuestionDao
) : IQuestionLocalSource {

    override suspend fun addQuestions(questions: List<Question>): Result<Unit> {
        return try {
            Result.Success(questionDao.insertAll(questions.map { it.toData() }))
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }

    override fun getQuestions(): Flow<List<Question>> {
        return questionDao.getAll().map { dQuestions ->
            dQuestions.map { it.toDomain() }
        }
    }

    override fun getQuestion(id: Int): Flow<Question?> {
        return questionDao.get(id).map { it?.toDomain() }.onEach { Timber.d("On each $it") }
    }

}

interface IQuestionLocalSource {
    suspend fun addQuestions(questions: List<Question>): Result<Unit>

    fun getQuestion(id: Int): Flow<Question?>

    fun getQuestions(): Flow<List<Question>>
}
