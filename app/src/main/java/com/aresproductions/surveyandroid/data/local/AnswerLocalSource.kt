package com.aresproductions.surveyandroid.data.local

import com.aresproductions.surveyandroid.data.local.dao.DAnswerDao
import com.aresproductions.surveyandroid.data.local.model.toData
import com.aresproductions.surveyandroid.data.local.model.toDomain
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import javax.inject.Inject

class AnswerLocalSource @Inject constructor(
        private val answerDao: DAnswerDao
) : IAnswerLocalSource {

    override suspend fun addAnswer(answer: Answer): Result<Unit> {
        return try {
            Result.Success(answerDao.insert(answer.toData()))
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }

    override fun getAnswers(): Flow<List<Answer>> {
        return answerDao.getAll().map { dAnswers ->
            dAnswers.map { it.toDomain() }
        }
    }

    override fun getAnswer(id: Int): Flow<Answer?> {
        return answerDao.get(id).map { it?.toDomain() }.onEach { Timber.d("On each $it") }
    }
}

interface IAnswerLocalSource {
    suspend fun addAnswer(answer: Answer): Result<Unit>

    fun getAnswer(id: Int): Flow<Answer?>

    fun getAnswers(): Flow<List<Answer>>
}