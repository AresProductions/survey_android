package com.aresproductions.surveyandroid.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aresproductions.surveyandroid.domain.entities.Answer

@Entity(tableName = "answers")
data class DAnswer(
        @PrimaryKey
        @ColumnInfo
        val id: Int,
        @ColumnInfo
        val description: String
)

fun DAnswer.toDomain() = Answer(id, description)

fun Answer.toData() = DAnswer(id, description)