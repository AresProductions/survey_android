package com.aresproductions.surveyandroid.data

import com.aresproductions.surveyandroid.data.local.AnswerLocalSource
import com.aresproductions.surveyandroid.data.remote.remotesource.AnswerRemoteSource
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AnswerRepo @Inject constructor(
        private val answerRemoteSource: AnswerRemoteSource,
        private val answerLocalSource: AnswerLocalSource
) : IAnswerRepo {
    override suspend fun post(answer: Answer): Result<Unit> {
        return when (val response = answerRemoteSource.postAnswer(answer)) {
            is Result.Success -> {
                answerLocalSource.addAnswer(answer)
            }
            is Result.Error -> {
                return Result.Error(response.throwable)
            }
        }
    }

    override fun getAll(): Flow<List<Answer>> {
        return answerLocalSource.getAnswers()
    }

    override fun get(id: Int): Flow<Answer?> {
        return answerLocalSource.getAnswer(id)
    }
}