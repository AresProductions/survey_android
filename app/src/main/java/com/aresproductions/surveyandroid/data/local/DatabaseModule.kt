package com.aresproductions.surveyandroid.data.local

import android.content.Context
import androidx.room.Room
import com.aresproductions.surveyandroid.data.local.dao.DAnswerDao
import com.aresproductions.surveyandroid.data.local.dao.DQuestionDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideQuestionDao(database: QuestionAnswerDatabase): DQuestionDao {
        return database.questionDao()
    }

    @Provides
    @Singleton
    fun provideAnswerDao(database: QuestionAnswerDatabase): DAnswerDao {
        return database.answerDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): QuestionAnswerDatabase {
        return Room.databaseBuilder(
                appContext,
                QuestionAnswerDatabase::class.java,
                QuestionAnswerDatabase.DATABASE_NAME
        ).build()
    }
}