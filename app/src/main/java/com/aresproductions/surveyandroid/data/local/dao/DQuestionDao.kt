package com.aresproductions.surveyandroid.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aresproductions.surveyandroid.data.local.model.DQuestion
import kotlinx.coroutines.flow.Flow

@Dao
interface DQuestionDao {
    @Query("SELECT * FROM questions")
    fun getAll(): Flow<List<DQuestion>>

    @Query("SELECT * FROM questions WHERE id=:id ")
    fun get(id: Int): Flow<DQuestion?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(questions: List<DQuestion>)
}