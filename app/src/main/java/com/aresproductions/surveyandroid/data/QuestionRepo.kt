package com.aresproductions.surveyandroid.data

import com.aresproductions.surveyandroid.data.local.QuestionLocalSource
import com.aresproductions.surveyandroid.data.remote.remotesource.QuestionRemoteSource
import com.aresproductions.surveyandroid.domain.entities.Question
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.repository.IQuestionRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class QuestionRepo @Inject constructor(
        private val questionRemoteSource: QuestionRemoteSource,
        private val questionLocalSource: QuestionLocalSource
) : IQuestionRepo {
    override suspend fun sync(): Result<Unit> {
        return when (val response = questionRemoteSource.getQuestions()) {
            is Result.Success -> {
                questionLocalSource.addQuestions(response.data)
            }
            is Result.Error -> {
                return Result.Error(response.throwable)
            }
        }
    }

    override fun getAll(): Flow<List<Question>> {
        return questionLocalSource.getQuestions()
    }

    override fun get(id: Int): Flow<Question?> {
        return questionLocalSource.getQuestion(id)
    }

}