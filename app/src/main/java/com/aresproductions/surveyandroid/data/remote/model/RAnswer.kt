package com.aresproductions.surveyandroid.data.remote.model

import com.aresproductions.surveyandroid.domain.entities.Answer

data class RAnswer(
        val id: Int,
        val answer: String
)

fun Answer.toRemote() = RAnswer(id = id, answer = description)