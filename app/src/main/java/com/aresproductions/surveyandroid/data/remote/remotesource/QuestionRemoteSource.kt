package com.aresproductions.surveyandroid.data.remote.remotesource

import com.aresproductions.surveyandroid.data.remote.QuestionAnswerApiService
import com.aresproductions.surveyandroid.data.remote.model.toDomain
import com.aresproductions.surveyandroid.domain.entities.Question
import com.aresproductions.surveyandroid.domain.entities.Result
import javax.inject.Inject

class QuestionRemoteSource @Inject constructor(
        private val api: QuestionAnswerApiService
) : IQuestionRemoteSource {
    override suspend fun getQuestions(): Result<List<Question>> {
        return try {
            Result.Success(api.getQuestions().map { it.toDomain() })
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }
}

interface IQuestionRemoteSource {
    suspend fun getQuestions(): Result<List<Question>>
}