package com.aresproductions.surveyandroid.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aresproductions.surveyandroid.data.local.model.DAnswer
import kotlinx.coroutines.flow.Flow

@Dao
interface DAnswerDao {
    @Query("SELECT * FROM answers")
    fun getAll(): Flow<List<DAnswer>>

    @Query("SELECT * FROM answers WHERE id=:id ")
    fun get(id: Int): Flow<DAnswer?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(answer: DAnswer)
}