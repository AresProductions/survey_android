package com.aresproductions.surveyandroid.data.remote.remotesource

import com.aresproductions.surveyandroid.data.remote.QuestionAnswerApiService
import com.aresproductions.surveyandroid.data.remote.model.toRemote
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import javax.inject.Inject

class AnswerRemoteSource @Inject constructor(
        private val api: QuestionAnswerApiService
) : IAnswerRemoteSource {
    override suspend fun postAnswer(answer: Answer): Result<Unit> {
        return try {
            Result.Success(api.postAnswer(answer.toRemote()))
        } catch (t: Throwable) {
            Result.Error(t)
        }
    }
}

interface IAnswerRemoteSource {
    suspend fun postAnswer(answer: Answer): Result<Unit>
}