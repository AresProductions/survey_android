package com.aresproductions.surveyandroid.data

import com.aresproductions.surveyandroid.domain.repository.IAnswerRepo
import com.aresproductions.surveyandroid.domain.repository.IQuestionRepo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface RepoModule {
    @get:Binds
    val AnswerRepo.iAnswerRepo: IAnswerRepo

    @get:Binds
    val QuestionRepo.iQuestionRepo: IQuestionRepo
}