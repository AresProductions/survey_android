package com.aresproductions.surveyandroid.data.remote.model

import com.aresproductions.surveyandroid.domain.entities.Question

data class RQuestion(
        val id: Int,
        val question: String
)

fun RQuestion.toDomain() = Question(id = id, description = question)
