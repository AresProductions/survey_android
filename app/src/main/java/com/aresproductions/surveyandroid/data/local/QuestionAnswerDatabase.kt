package com.aresproductions.surveyandroid.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.aresproductions.surveyandroid.data.local.dao.DAnswerDao
import com.aresproductions.surveyandroid.data.local.dao.DQuestionDao
import com.aresproductions.surveyandroid.data.local.model.DAnswer
import com.aresproductions.surveyandroid.data.local.model.DQuestion

@Database(entities = [DAnswer::class, DQuestion::class], version = 1, exportSchema = false)
abstract class QuestionAnswerDatabase : RoomDatabase() {
    abstract fun answerDao(): DAnswerDao
    abstract fun questionDao(): DQuestionDao

    companion object {
        const val DATABASE_NAME = "questionsAnswers.db"
    }
}