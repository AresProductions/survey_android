package com.aresproductions.surveyandroid.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aresproductions.surveyandroid.domain.entities.Question


@Entity(tableName = "questions")
data class DQuestion(
        @PrimaryKey
        @ColumnInfo
        val id: Int,
        @ColumnInfo
        val description: String
)

fun DQuestion.toDomain() = Question(id, description)

fun Question.toData() = DQuestion(id, description)