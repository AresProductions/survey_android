package com.aresproductions.surveyandroid

import com.aresproductions.surveyandroid.data.AnswerRepo
import com.aresproductions.surveyandroid.data.QuestionRepo
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Question
import com.aresproductions.surveyandroid.domain.entities.QuestionWithAnswer
import com.aresproductions.surveyandroid.domain.usecase.GetQuestionsWithAnswersUC
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetQuestionsAnswerUCTests {

    private val answerRepo = mockk<AnswerRepo>()
    private val questionRepo = mockk<QuestionRepo>()
    private val useCase = GetQuestionsWithAnswersUC(answerRepo, questionRepo)

    @Test
    fun `given repos returns a list of answers and questions, use case returns questionsAndAnswers list`() = runBlocking {
        // given
        val answerList = listOf(Answer(0, "answer"))
        val questionList =
            listOf(Question(0, "question"), Question(2, "question2"), Question(3, "question3"))
        every { answerRepo.getAll() } returns flowOf(answerList)
        every { questionRepo.getAll() } returns flowOf(questionList)

        val expectedList = listOf(
            QuestionWithAnswer(
                questionList[0].id,
                questionList[0].description,
                answerList[0].description
            ),
            QuestionWithAnswer(
                questionList[1].id,
                questionList[1].description,
                null
            ),
            QuestionWithAnswer(
                questionList[2].id,
                questionList[2].description,
                null
            )

        )
        // when, then
        useCase.execute().collect {
            Assert.assertEquals(it, expectedList)
        }
        verify { answerRepo.getAll() }
        verify { questionRepo.getAll() }
    }

}