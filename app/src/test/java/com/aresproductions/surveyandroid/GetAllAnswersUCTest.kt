package com.aresproductions.surveyandroid

import com.aresproductions.surveyandroid.data.AnswerRepo
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.usecase.GetAllAnswersUC
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test


class GetAllAnswersUCTest {

    private val answerRepo = mockk<AnswerRepo>()
    private val useCase = GetAllAnswersUC(answerRepo)

    @Test
    fun `given repo returns a list, use case succeeds`() = runBlocking {
        // given
        val answerList = listOf(Answer(0, ""))
        every { answerRepo.getAll() } returns flowOf(answerList)

        // when, then
        useCase.execute().collect {
            assertEquals(it, answerList)
        }
        verify { answerRepo.getAll() }
    }

}