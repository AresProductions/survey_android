package com.aresproductions.surveyandroid

import com.aresproductions.surveyandroid.data.AnswerRepo
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import com.aresproductions.surveyandroid.domain.usecase.PostAnswerUC
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class PostAnswerUCTests {

    private val answerRepo = mockk<AnswerRepo>()
    private val useCase = PostAnswerUC(answerRepo)

    @Test
    fun `given Answer, when postAnswer succeeds, then return success`() = runBlocking {
        // given
        val answer = Answer(0, "answer")
        val result = Result.Success(Unit)
        coEvery { answerRepo.post(answer) } returns result


        // when, then
        Assert.assertEquals(useCase.execute(answer), result)
        coVerify { answerRepo.post(answer) }
    }

    @Test
    fun `given Answer, when postAnswer fails, then return error`() = runBlocking {
        // given
        val answer = Answer(0, "answer")
        val throwable = Throwable()
        val result = Result.Error(throwable)
        coEvery { answerRepo.post(answer) } returns result


        // when, then
        Assert.assertEquals(useCase.execute(answer), result)
        coVerify { answerRepo.post(answer) }
    }

}