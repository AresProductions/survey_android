package com.aresproductions.surveyandroid

import com.aresproductions.surveyandroid.data.AnswerRepo
import com.aresproductions.surveyandroid.data.local.AnswerLocalSource
import com.aresproductions.surveyandroid.data.remote.remotesource.AnswerRemoteSource
import com.aresproductions.surveyandroid.domain.entities.Answer
import com.aresproductions.surveyandroid.domain.entities.Result
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class AnswerRepoTests {

    private val answerRemoteSource = mockk<AnswerRemoteSource>()
    private val answerLocalSource = mockk<AnswerLocalSource>()

    private val repo = AnswerRepo(answerRemoteSource, answerLocalSource)

    @Test
    fun `given Answer, when both sources succeed, then return success`() = runBlocking {
        // given
        val answer = Answer(0, "answer")
        val remoteResult = Result.Success(Unit)
        val localResult = Result.Success(Unit)
        val expectedResult = Result.Success(Unit)
        coEvery { answerRemoteSource.postAnswer(answer) } returns remoteResult
        coEvery { answerLocalSource.addAnswer(answer) } returns localResult

        // when, then
        Assert.assertEquals(repo.post(answer), expectedResult)
        coVerify { answerRemoteSource.postAnswer(answer) }
        coVerify { answerLocalSource.addAnswer(answer) }

    }

    @Test
    fun `given Answer, when remoteSource fails, then return error`() = runBlocking {
        // given
        val throwable = Throwable()
        val answer = Answer(0, "answer")
        val remoteResult = Result.Error(throwable)
        coEvery { answerRemoteSource.postAnswer(answer) } returns remoteResult

        // when, then
        Assert.assertEquals(repo.post(answer), remoteResult)
        coVerify { answerRemoteSource.postAnswer(answer) }
        verify { answerLocalSource wasNot Called }
    }

    @Test
    fun `given Answer, when dataSource fails, then return error`() = runBlocking {
        // given
        val throwable = Throwable()
        val answer = Answer(0, "answer")
        val remoteResult = Result.Success(Unit)
        val localResult = Result.Error(throwable)
        coEvery { answerRemoteSource.postAnswer(answer) } returns remoteResult
        coEvery { answerLocalSource.addAnswer(answer) } returns localResult

        // when, then
        Assert.assertEquals(repo.post(answer), localResult)
        coVerify { answerRemoteSource.postAnswer(answer) }
        coVerify { answerLocalSource.addAnswer(answer) }
    }

}