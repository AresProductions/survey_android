# Survey App Android

### Technologies
* Language Used: Kotlin
* Libraries: Room, Coroutines, Flow, Navigation Component, Mockk
* Android Target: API 21+

### Architecture
* General Architecture: Clean Architecture
* Data Layer: Remote Source saves data on the Database which is the Single Source of Truth for the whole project
* Presentation Layer: MVVM
* Testing: Only Unit Testing
